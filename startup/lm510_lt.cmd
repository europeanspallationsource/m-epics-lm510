# @field DEVICENAME
# @type STRING
# Device name, should be the same as the name in CCDB

# @field CONTROLLER
# @type STRING
# Name of the head unit (as in CCDB)

# @field CHANNEL
# @type INTEGER
# Channel on the head unit where the level transmitter is connected to (1, 2)


#Load the database defining your EPICS records
dbLoadRecords(lm510_lt.db, "P = $(DEVICENAME), R = :, CONTROLLER = $(CONTROLLER), CH = $(CHANNEL), ASYNPORT = $(CONTROLLER)")
